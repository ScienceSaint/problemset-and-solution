/* The maximum value of k should be determined in the following way:
 let's find the maximum number of people already sitting on the same bench (i. e. the maximum value in the array a).
  Let this number be t. Then if all additional m people will seat on this bench, we will get the maximum value of k, so the answer is t+m

To determine the minimum value of k
let's perform m operations. 
During each operation we put a new person on the bench currently having minimum number of people occupying it. 
The answer is the maximum number of people on the bench after we perform this operation for each of m newcomers.*/
#include<iostream>
using namespace std;
int main()
{
	int n, m, a[100],b[100], count, min, max;
	cin>>n;
	cin>>m;
	for(int j = 0 ; j < n ; j++)
		cin>>a[j];
	for(int i = 0 ; i < n ; i++)
	{
		b[i] = a[i];
	}
	//finding the minimum k
	min =a[0];
	for(int x = 0 ; x < m; x++)
	{
		for(int i = 0 ; i < n ; i++)
		{
			if(min > a[i])
				min = a[i];
		}
		for (int i = 0; i < n ; i++)
		{
			if(min == a[i])
			{
				a[i]++;
				min = a[i];
				break;
			}
		}
	}
	cout<<a[0];
	//finding the maximum k
	int t = b[0];
	for(int i = 0 ; i < n ; i++)
	{
		if(t < b[i])
			t = b[i];
	}
	max = t + m;
	cout<<" "<<max;

	
	return 0;
}