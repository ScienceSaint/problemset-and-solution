# Problemset and Solution

Hi, I am Science Saint. This repository contains solution to some problem set. Five problem sets and solutions will be added every week.
This problem sets are solved by me. Feel free to add your comments and if you have any better solution for the problem set, feel free to pull request.


# License
This work is licensed under a Mozilla Public License(MPL).